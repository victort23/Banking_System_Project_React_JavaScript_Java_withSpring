package com.example.Banking_System_Project.repositories;

import com.example.Banking_System_Project.entities.Account;
import com.example.Banking_System_Project.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    Optional<Account> findByIdClient(int id);
}
