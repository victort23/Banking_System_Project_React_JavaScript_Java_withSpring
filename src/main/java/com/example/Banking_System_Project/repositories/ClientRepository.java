package com.example.Banking_System_Project.repositories;

import com.example.Banking_System_Project.entities.Account;
import com.example.Banking_System_Project.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Integer> {

    Optional<Client> findByUsername(String username);
}
