package com.example.Banking_System_Project.controllers;

import com.example.Banking_System_Project.entities.Account;
import com.example.Banking_System_Project.entities.Client;
import com.example.Banking_System_Project.models.Login;
import com.example.Banking_System_Project.services.AccountService;
import com.example.Banking_System_Project.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("getAccount")
    public ResponseEntity<Integer> getAccount(@Valid @RequestBody int idClient) {
        int userId = accountService.findAccountByClientId(idClient);
        if(userId!= -1){
            return new ResponseEntity<>(userId, HttpStatus.CREATED);
        }

        return new ResponseEntity<>(-1, HttpStatus.CREATED);
    }

    @GetMapping("/getAccounts")
    public ResponseEntity<List<Account>> getClients() {
        List<Account> dtos = accountService.findAccounts();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("insertAccount")
    public ResponseEntity<Integer> insertClient(@Valid @RequestBody Account personDTO) {
        int personID = accountService.insertAccount(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

}
