package com.example.Banking_System_Project.controllers;

import com.example.Banking_System_Project.entities.Client;
import com.example.Banking_System_Project.models.Login;
import com.example.Banking_System_Project.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/client")
public class ClientController {

    private final ClientService userService;

    @Autowired
    public ClientController(ClientService userService) {
        this.userService = userService;
    }

    @PostMapping("login")
    public ResponseEntity<Integer> insertUser(@Valid @RequestBody Login login) {
        int userId = userService.findClientByUsernameAndPass(login);
        if(userId!= -1){
            return new ResponseEntity<>(userId, HttpStatus.CREATED);
        }

        return new ResponseEntity<>(-1, HttpStatus.CREATED);
    }

    @GetMapping("/getClients")
    public ResponseEntity<List<Client>> getClients() {
        List<Client> dtos = userService.findClients();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("insertUser")
    public ResponseEntity<Integer> insertClient(@Valid @RequestBody Client personDTO) {
        int personID = userService.insertClient(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

}
