package com.example.Banking_System_Project.services;


import com.example.Banking_System_Project.entities.Client;
import com.example.Banking_System_Project.models.Login;
import com.example.Banking_System_Project.repositories.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Transactional
@Service
public class ClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(com.example.Banking_System_Project.services.ClientService.class);
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository=clientRepository;
    }

    public Client findClientById(int id) {
        Optional<Client> clientOptional = clientRepository.findById(id);
        if (clientOptional == null) {
            LOGGER.error("Person with id {} was not found in db", id);
            // throw new ResourceNotFoundException(Client.class.getSimpleName() + " with id: " + id);
        }
        return clientOptional.get();
    }

    public int insertClient(Client client) {
        Client person = clientRepository.save(client);
        LOGGER.debug("Client with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public int findClientByUsernameAndPass(Login login) {
        Optional<Client> prosumerOptional = clientRepository.findByUsername(login.getUsername());
        if (!prosumerOptional.isPresent()) {
            return -1;
        }else
        if(!prosumerOptional.get().getPassword().equals(login.getPassword())){
            return -1;
        }
        return prosumerOptional.get().getId();
    }
    public List<Client> findClients() {
        List<Client> clientsList = clientRepository.findAll();
        return clientsList;
    }

}
