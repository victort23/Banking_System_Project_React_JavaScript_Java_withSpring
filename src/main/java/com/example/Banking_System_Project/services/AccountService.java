package com.example.Banking_System_Project.services;

import com.example.Banking_System_Project.entities.Account;
import com.example.Banking_System_Project.entities.Client;
import com.example.Banking_System_Project.models.Login;
import com.example.Banking_System_Project.repositories.AccountRepository;
import com.example.Banking_System_Project.repositories.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(com.example.Banking_System_Project.services.ClientService.class);
    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository clientRepository) {
        this.accountRepository=clientRepository;
    }

    public Account findAccountById(int id) {
        Optional<Account> clientOptional = accountRepository.findById(id);
        if (clientOptional == null) {
            LOGGER.error("Person with id {} was not found in db", id);
            // throw new ResourceNotFoundException(Client.class.getSimpleName() + " with id: " + id);
        }
        return clientOptional.get();
    }

    public int insertAccount(Account client) {
        Account person = accountRepository.save(client);
        LOGGER.debug("Client with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public int findAccountByClientId(int id) {
        Optional<Account> prosumerOptional = accountRepository.findByIdClient(id);
        if (!prosumerOptional.isPresent()) {
            return -1;
        }
        return prosumerOptional.get().getId();
    }
    public List<Account> findAccounts() {
        List<Account> clientsList = accountRepository.findAll();
        return clientsList;
    }
}
