package com.example.Banking_System_Project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankingSystemProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankingSystemProjectApplication.class, args);
	}

}
